from st3m.application import Application, ApplicationContext
import st3m.run
import leds
import os
import sys

#inspired / adopted from mot-Antifa, and flow3r-documentation

class MOiN(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.image_path = f"{self.app_ctx.bundle_path}/moin.png"

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.image(self.image_path, -120, -30, 240, 60)

        for i in range(40):
            leds.set_rgb(i, 0, 255, 0)

        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
    st3m.run.run_view(MOiN(ApplicationContext()))
