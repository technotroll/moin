# MOiN

This app shows the MOiN logo on the CCCamp23 flow3r badge screen and sets all LEDs to green.

MOiN means "Mehrere Orte im Norden" (Multiple Locations in the North) and includes
- Chaostreff Flensburg
- Chaostreff Kiel
- Chaotikum Lübeck
- Hacklabor Schwerin
- Port39 Stralsund